# Ethical Contributor Licence Agreement

Use this CLA to ensure that

- merging contributions constitutes no copyright infringement
- you can switch to a new licence similar to the current one without having to ask every single contributor for permission.

## Licence

This CLA may be used under the [CC BY-SA 4.0 licence](LICENCE).